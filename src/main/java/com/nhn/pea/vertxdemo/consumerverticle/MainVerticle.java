package com.nhn.pea.vertxdemo.consumerverticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;

public class MainVerticle extends AbstractVerticle {

	@Override
	public void start(Future<Void> fut) {
				
		final Router router = Router.router(vertx);
		
		Route route = router.route().path("/ps");
		route.handler(routingContext -> {
			System.out.println("++PubSub Handle");
			routingContext.response().end("<h1>PubSub Handle</h1>");
			
			vertx.eventBus().publish("MSG_PS", "msg published from PeaPubSub");

		});

		route = router.route().path("/p2p");
		route.handler(routingContext -> {
			System.out.println("++Point2Point Handle");
			routingContext.response().end("<h1>Point2Point Handle</h1>");
			
			vertx.eventBus().send("MSG_P2P", "msg sent from PeaPoint2Point", reply -> {
				if (reply.succeeded()) {
					System.out.println("Received reply " + reply.result().body());
				} else {
					System.out.println("No reply");
				}
			});
		});

		HttpServer httpServer = vertx.createHttpServer();
		httpServer.requestHandler(router::accept).listen(8080);
		
		

	}

}
