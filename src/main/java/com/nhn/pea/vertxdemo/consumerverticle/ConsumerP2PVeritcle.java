
package com.nhn.pea.vertxdemo.consumerverticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class ConsumerP2PVeritcle extends AbstractVerticle {
	private static int verticleNum = 0;
	private String verticleName;

	public ConsumerP2PVeritcle() {
		verticleNum++;
		this.verticleName = " ConsumerP2PVeritcles " + verticleNum;
	}

	@Override
	public void start(Future<Void> startFuture) {

		vertx.eventBus().consumer("MSG_P2P", message -> {
			System.out.println(verticleName + " receive Msg: " + message.body());
			message.reply("pong!");
		});
	}
}
