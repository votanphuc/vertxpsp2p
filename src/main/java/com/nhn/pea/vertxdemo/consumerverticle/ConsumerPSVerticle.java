package com.nhn.pea.vertxdemo.consumerverticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class ConsumerPSVerticle extends AbstractVerticle {
	private static int verticleNum = 0;
	private String verticleName;

	public ConsumerPSVerticle() {
		verticleNum++;
		this.verticleName = " ConsumerPSVerticle " + verticleNum;
	}

	@Override
	public void start(Future<Void> startFuture) {
		vertx.eventBus().consumer("MSG_PS", message -> {
			System.out.println(verticleName + " receive Msg: " + message.body());
		});
	}
}
