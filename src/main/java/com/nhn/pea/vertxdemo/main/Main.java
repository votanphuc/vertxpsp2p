package com.nhn.pea.vertxdemo.main;

import com.nhn.pea.vertxdemo.consumerverticle.ConsumerP2PVeritcle;
import com.nhn.pea.vertxdemo.consumerverticle.ConsumerPSVerticle;
import com.nhn.pea.vertxdemo.consumerverticle.MainVerticle;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

/**
 * Hello world!
 *
 */
public class Main {
	public static void main(String[] args) {
		System.out.println("=========START========");
		
		Vertx vertx = Vertx.vertx();
		DeploymentOptions deployOptions = new DeploymentOptions().setInstances(1);
		
		vertx.deployVerticle(ConsumerPSVerticle.class.getName(), deployOptions);
		vertx.deployVerticle(ConsumerP2PVeritcle.class.getName(),deployOptions);
		vertx.deployVerticle(MainVerticle.class.getName());
	}
}
